import { getAccessToken, logout } from "@/auth/jwtService";
import axios from "axios";
import { useRoute } from "vue-router";
const route = useRoute();

import { toast } from "vue3-toastify";

const axiosIns = axios.create({
  baseURL: import.meta.env.VITE_BACKEND,
  timeout: 20000,

  headers: { Accept: "application/json" },
});

const axiosIns2 = axios.create({
  baseURL: import.meta.env.VITE_BACKEND_2,
  timeout: 20000,
});

//send token
axiosIns.interceptors.request.use(
  (config) => {
    let token = getAccessToken();
    if (token && config.headers) {
      config.headers.Authorization = "Bearer " + token;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

//404 or 401 pages use this response
axiosIns.interceptors.response.use(
  (response) => response,
  (error) => {
    axiosIns2({
      url: "project/statistic_card_history/create_error_log/",
      method: "POST",
      data: {
        method: error.response.config.method,
        base_url: error.response.config.baseURL + error.response.config.url,
        message: error.response.statusText,
        status: error.response.status,
        params: error.response.config.params,
        data: error.response.data,
        headers: error.response.headers,
        project_name: "Sott",
        project_id: 79,
        user_id: 1,
      },
    })
      .then((res) => {
        // commit('SET_APPLICATION_PURCHASE', res.data)
        console.log(res.data);
      })
      .catch((e) => {
        console.log(e);
      });

    console.log(error);
    console.log(error.response);
    // TODO надо исправит refresh токен
    if (
      error.response &&
      error.response.status === 401 &&
      route.meta.layout !== "LoginLayout"
    ) {
      logout();
    } else if (error.response.status === 422) {
      toast.error("error");
      if (
        error.response &&
        error.response.data &&
        error.response.data.message
      ) {
        toast.error(error.response.data.message);
      }
    } else if (error.response.status === 400) {
      toast.error("Ошибка!");
      if (
        error.response &&
        error.response.data &&
        error.response.data.message
      ) {
        toast.error(error.response.data.message);
      }
    } else if (error.response.status === 500) {
      toast.error("Error from server!");
    }

    return Promise.reject(error);
  }
);

export default axiosIns;
