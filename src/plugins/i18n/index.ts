import { createI18n } from "vue-i18n";

const messages = Object.fromEntries(
  Object.entries(import.meta.glob("./locales/*.json", { eager: true })).map(
    ([key, value]: any) => [key.slice(10, -5), value.default]
  )
);

export default createI18n({
  legacy: false,
  locale: "ru",
  allowComposition: true,
  fallbackLocale: "ru",
  messages,
  globalInjection: true,
});

//  TODO надо проверить работает ли нет i18n
