export default {
  methods: {
    $_formatPriceMixin(value) {
      if (value) {
        let currency = value.toString().replace(/,/g, "")
        currency = parseFloat(currency)
        currency = new Intl.NumberFormat("en-US", {
          minimumFractionDigits: 0,
          maximumFractionDigits: 2,
        }).format(currency)

        return currency
      }
      
      return 0
    },
  },
}
