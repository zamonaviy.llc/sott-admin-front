import toast from '@core/components/toastification/ToastificationContent.vue'

export default {
  data() {
    return {
      _initTimeout: null,
    }
  },

  // уведомления
  methods: {
    async $_okToast() {
      // eslint-disable-next-line no-new-object
      const typeToast = new Object()

      typeToast.status = 'success'
      typeToast.title = this.$t('success')
      typeToast.icon = 'CheckIcon'
      typeToast.text = ''

      this.$toast(
        {
          component: toast,
          props: {
            title: typeToast.title,
            icon: typeToast.icon,
            text: typeToast.text,
            variant: 'success',
          },
        },
        {
          position: 'bottom-right',
        },
      )
    },
    async $_errToast({ message }) {
      // eslint-disable-next-line no-new-object
      const typeToast = new Object()

      typeToast.status = 'danger'
      typeToast.title = this.$t('error')
      typeToast.icon = 'AlertCircleIcon'
      typeToast.text = message || ''

      this.$toast(
        {
          component: toast,
          props: {
            title: typeToast.title,
            icon: typeToast.icon,
            text: typeToast.text,
            variant: 'danger',
          },
        },
        {
          position: 'bottom-right',
        },
      )
    },
    async $_warningToast(message) {
      // eslint-disable-next-line no-new-object
      const typeToast = new Object()

      typeToast.status = 'danger'
      typeToast.title = message || ''
      typeToast.icon = 'AlertCircleIcon'

      this.$toast(
        {
          component: toast,
          props: {
            title: typeToast.title,
            icon: typeToast.icon,
            variant: 'warning',
          },
        },
        {
          position: 'bottom-right',
        },
      )
    },
  },

  // !уведомления
}
