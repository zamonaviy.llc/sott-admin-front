import useJwt from "@/auth/jwt/useJwt"

export default {
  methods: {
    checkButtonPermission(roles) {
      const role = useJwt.getUserData().role

      // if (role?.includes('admin')) {
      //   return true
      // }
      return roles.some(item => role?.includes(item))
    },
  },
}
