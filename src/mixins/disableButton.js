export default {
  methods: {
    disableButton(event) {
      // Disable the button
      event.target.disabled = true

      // Enable the button after a 2 second delay
      setTimeout(() => {
        event.target.disabled = false
      }, 2000)
    },
  },
}
