export default {
  methods: {
    moneyFormatter(num) {
      return new Intl.NumberFormat("ru-Ru", {
        style: "currency",
        currency: "UZS",
      }).format(num)
    },
    dateTimeFormatter(date) {
      return new Date(date).toLocaleString()
    },
  },
}
