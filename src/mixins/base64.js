export default {
  methods: {
    $_convertToBase64(element) {
      return new Promise((resolve, reject) => {
        const file = element
        const reader = new FileReader()

        reader.readAsDataURL(file)
        reader.onloadend = function () {
          resolve(reader.result)
        }
        reader.onerror = error => reject(error)
      })
    },
  },
}
