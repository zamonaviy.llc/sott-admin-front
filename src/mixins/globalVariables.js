export default {
  data() {
    return {
      type_money_options: [
        {
          id: "cash",
          title: {
            uz: "Naxt",
            ru: "Наличный",
          },
        },
        {
          id: "bank",
          title: {
            uz: "Pul o'tkazma",
            ru: "Перечисления",
          },
        },
      ],
    }
  },
}
