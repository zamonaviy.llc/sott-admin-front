export default {
  methods: {
    $_removeObjectConnection(obj) {
      if (typeof obj !== "function") {
        return JSON.parse(JSON.stringify(obj))
      }
      
      return undefined
    },

    $_objectToFormData(obj, form, namespace) {
      const fd = form || new FormData()
      let formKey
      // eslint-disable-next-line no-restricted-syntax
      for (const property in obj) {
        // eslint-disable-next-line no-prototype-builtins
        if (obj.hasOwnProperty(property)) {
          if (namespace) {
            formKey = `${namespace}.${property}`
          } else {
            formKey = property
          }

          // if the property is an object, but not a File,
          // use recursivity.
          if (Array.isArray(obj[property])) {
            obj[property].forEach(e => {
              fd.append(property, e)
            })
          } else if (
            typeof obj[property] === "object" &&
            !(obj[property] instanceof File)
          ) {
            this.$_objectToFormData(obj[property], fd, property)
          } else if (obj[property]) {
            // if it's a string or a File object
            fd.append(formKey, obj[property])
          }
        }
      }

      return fd
    },
  },
}
