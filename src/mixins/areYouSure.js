export default {
  methods: {
    $_showAreYouSureModal() {
      return this.$bvModal.msgBoxConfirm(this.$t("do_you_really_want_delete"), {
        title: this.$t("confirm"),
        okVariant: "danger",
        okTitle: this.$t("yes"),
        cancelTitle: this.$t("no"),
        hideHeaderClose: false,
        centered: true,
      })
    },
  },
}
