export default {
  computed: {
    $getLang() {
      return this.$i18n.locale || "ru"
    },
  },
}
