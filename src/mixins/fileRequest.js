/* eslint-disable guard-for-in */
export default {
  methods: {
    $_convertToFormdata(item) {
      const form_data = new FormData()
      // eslint-disable-next-line no-restricted-syntax
      for (const key in item) {
        form_data.append(key, item[key])
      }
      
      return form_data
    },
  },
}
