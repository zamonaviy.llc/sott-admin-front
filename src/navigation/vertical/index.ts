import adminstrator from "./adminstrator";

export default [
  {
    header: "Справочник",
    name: "Справочник",
    icon: "cog",
    path: "/home",
    child: [
      {
        name: "Forms",
        icon: "album",
        path: "/forms",
      },
      {
        name: "Listing types",
        icon: "thumbnails",
        path: "/listing-types",
      },
      {
        name: "Social links",
        icon: "social",
        path: "/social-links",
      },

      {
        name: "Interest",
        icon: "bookmark",
        path: "/interest",
      },

      {
        name: "Nearby Places",
        icon: "location",
        path: "/nearby-places",
      },

      {
        name: "Types of Places",
        icon: "location",
        path: "/types-places",
      },

      {
        name: "Contacts",
        icon: "list",
        path: "/contact-us",
      },
    ],
  },
  ...adminstrator,
];
