export default [
  {
    header: "Пользователи",
    name: "Пользователи",
    icon: "users",
    path: "/users",
  },

  {
    header: "Персонал",
    name: "Персонал",
    icon: "users",
    path: "/staff",
  },
];
