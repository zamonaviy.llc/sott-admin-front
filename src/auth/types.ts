export type UserData = {
  id: number
  username: string | null
  is_superuser: boolean
  language: string | null
  theme: string | null
  role: object[]
  photo: string | null
}