
// Endpoints here
export const loginEndpoint: string = '/users/login'
export const registerEndpoint: string = '/users/register/'
export const logoutEndpoint: string = ''
export const refreshEndpoint: string = '/users/login/refresh'

// KeyNames here
export const tokenKeyName = 'access'
export const refreshTokenKeyName = 'refresh'