import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
// import { canNavigate } from "@/layout/plugins/casl";
import { isUserLoggedIn } from "./utils";
import adminstrator from "./adminstrator";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: () => {
      if (isUserLoggedIn()) {
        return { name: "home" };
      }
      return { name: "login" };
    },
  },
  {
    path: "/home",
    name: "home",
    component: () => import("@/modules/Reference/Reference.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Справочник",
          active: false,
          // to: "/",
        },
      ],
    },
  },

  {
    path: "/forms",
    name: "forms",
    component: () => import("@/modules/Reference/Forms.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Forms",
          active: false,
          // to: "/forms",
        },
      ],
    },
  },

  {
    path: "/listing-types",
    name: "listing-types",
    component: () => import("@/modules/Reference/ListingTypes.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Listing types",
          active: false,
          // to: "/listing-types",
        },
      ],
    },
  },

  {
    path: "/listing-detail/:id",
    name: "listing-detail",
    component: () => import("@/modules/Reference/DetailsListingType.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Listing types",
          active: true,
          // to: "/listing-types",
        },

        {
          text: "add",
          active: false,
        },
      ],
    },
  },

  {
    path: "/add-form-type",
    name: "add-form-type",
    component: () => import("@/modules/Reference/AddingFormToType.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Forms",
          active: true,
          to: "/forms",
        },

        {
          text: "add",
          active: true,
        },
      ],
    },
  },

  {
    path: "/interest",
    name: "interest",
    component: () => import("@/modules/Reference/Interest/Interest.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Interest",
          active: false,
          // to: "/interest",
        },
      ],
    },
  },

  {
    path: "/social-links",
    name: "social-links",
    component: () => import("@/modules/Reference/SocialLinks.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Social links",
          active: false,
          // to: "/social-links",
        },
      ],
    },
  },

  {
    path: "/nearby-places",
    name: "nearby-places",
    component: () =>
      import("@/modules/Reference/NearbyPlaces/NearbyPlaces.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Nearby Places",
          active: false,
          // to: "/nearby-places",
        },
      ],
    },
  },

  {
    path: "/contact-us",
    name: "contact-us",
    component: () => import("@/modules/Reference/ContactUs/ContactUs.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Contacts",
          active: false,
          // to: "/contact-us",
        },
      ],
    },
  },

  {
    path: "/types-places",
    name: "types-places",
    component: () => import("@/modules/Reference/TypesPlaces/TypesPlaces.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Types of Places",
          active: false,
          // to: "/types-places",
        },
      ],
    },
  },

  {
    path: "/new-form",
    name: "new-form",
    component: () => import("@/modules/Reference/components/CreateForms.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Forms",
          active: true,
          to: "/forms",
        },

        {
          text: "add",
          active: true,
        },
      ],
    },
  },

  {
    path: "/form-detail/:id",
    name: "form-detail",
    component: () => import("@/modules/Reference/components/CreateForms.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Forms",
          active: true,
          to: "/forms",
        },

        {
          text: "edit",
          active: true,
        },
      ],
    },
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/Login.vue"),
    meta: {
      loginNotRequired: true,
      layout: "LoginLayout",
    },
  },
  {
    path: "/error-404",
    name: "error-404",
    component: () => import("@/views/error/Error404.vue"),
    meta: {
      loginNotRequired: true,
      layout: "EmptyLayout",
    },
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: "error-404",
  },
  ...adminstrator,
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach(async (to, _, next) => {
  const isLoggedIn = isUserLoggedIn();
  if (to.meta.loginNotRequired) return next();

  // if (!isLoggedIn) {
  //   return next({ name: "login" });
  // }
  next();
});

export default router;
