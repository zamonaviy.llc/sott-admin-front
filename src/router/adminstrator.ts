export default [
  {
    path: "/users",
    name: "users",
    component: () => import("@/modules/Users/Users.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Пользователи",
          active: true,
          // to: "/users",
        },
      ],
    },
  },

  {
    path: "/users-detail/:id",
    name: "users-detail",
    component: () => import("@/modules/Users/DetailUser.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Пользователи",
          active: true,
          // to: "/users",
        },

        {
          text: "edit",
          active: false,
          // to: "/users-detail",
        },
      ],
    },
  },

  {
    path: "/staff",
    name: "staff",
    component: () => import("@/modules/Staff/Staff.vue"),
    meta: {
      layout: "MainLayout",
      pageTitle: "",
      breadcrumb: [
        {
          text: "Персонал",
          active: false,
          // to: "/staff",
        },
      ],
    },
  },
];
