export default [
  {
    path: "/register",
    name: "register",
    component: () => import("@/views/registration/Register.vue"),
    meta: {
      layout: "LoginLayout",
      loginNotRequired: true,
    },
  },
  {
    path: "/register/fill-data",
    name: "fill-data",
    component: () => import("@/views/registration/FillData.vue"),
    meta: {
      layout: "LoginLayout",
      loginNotRequired: true,
    },
  },
  {
    path: "/register/sms-confirmation/",
    name: "sms-confirmation",
    component: () => import("@/views/registration/SMSConfirmation.vue"),
    meta: {
      layout: "LoginLayout",
      loginNotRequired: true,
    },
  },
  {
    path: "/register/face-scan",
    name: "face-scan",
    component: () => import("@/views/registration/FaceScan.vue"),
    meta: {
      layout: "LoginLayout",
      loginNotRequired: true,
    },
  },
  {
    path: "/register/set-password",
    name: "set-password",
    component: () => import("@/views/registration/SetPassword.vue"),
    meta: {
      layout: "LoginLayout",
      loginNotRequired: true,
    },
  },
  {
    path: "/register/choose-role",
    name: "chooseRole",
    component: () => import("@/views/registration/ChooseRole.vue"),
    meta: {
      layout: "LoginLayout",
    },
  },
  {
    path: "/register/fill-card-data",
    name: "fill-card-data",
    component: () => import("@/views/registration/FillCardData.vue"),
    meta: {
      layout: "LoginLayout",
    },
  },
  {
    path: "/register/confirm-card",
    name: "confirm-card",
    component: () => import("@/views/registration/ConfirmCard.vue"),
    meta: {
      layout: "LoginLayout",
    },
  },
  {
    path: "/register/data-sheet",
    name: "data-sheet",
    component: () => import("@/views/registration/VehicleDataSheet.vue"),
    meta: {
      layout: "LoginLayout",
    },
  },
  {
    path: "/register/registered-successfully",
    name: "registered-successfully",
    component: () => import("@/views/registration/RegisteredSuccessfully.vue"),
    meta: {
      layout: "LoginLayout",
    },
  },
];
