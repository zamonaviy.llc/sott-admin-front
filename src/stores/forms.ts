// stores/counter.js
import { defineStore } from "pinia";
import {
  createForms,
  getFormsList,
  getOneForm,
  updateOneForm,
} from "../api/forms";

export const useFormStore = defineStore("forms", {
  state: () => {
    return {
      forms_list: {
        data: {
          data: [],
        },
      },
      one_form: {
        data: {},
      },
    };
  },
  getters: {
    getFormsList: (state) => state.forms_list,
    getOneForm: (state) => state.one_form,
  },

  actions: {
    async AddForms(data) {
      return await createForms(data);
    },

    async getAllFormsList(params) {
      const res = await getFormsList(params);

      this.forms_list = res.data;
    },

    async getOneFormAction(id: string | string[]) {
      const res = await getOneForm(id);

      this.one_form.data = res.data;
    },

    async updateOneForm(data) {
      return await updateOneForm(data);
    },
  },
});
