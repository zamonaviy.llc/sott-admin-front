// import { postAttachment } from '@/api/directory'
import { defineStore } from "pinia";
import {
  createRealEstateTypesSubeType,
  getSubeType,
  deleteSubeType,
  updateSubeType,
  getSubeTypeRelationFormList,
  createSubTypeRelationForm,
  deleteSubTypeRelationForm,
  changeStatusSubTypeRelationForm,
} from "@/api/listingTypes";

export const listingTypes = defineStore("listingType", {
  // STATE

  state: () => ({
    loading: false,
    listingTypesRelationFormsList: {
      data: [],
    },

    listingRealEstateTypesSubeType: {
      data: [],
    },
  }),

  //   ACTIONS

  actions: {
    FETCH_SUBE_TYPE_RELATION_FORM_LIST(params) {
      return getSubeTypeRelationFormList(params).then((res) => {
        this.listingTypesRelationFormsList = res.data;
      });
    },

    FETCH_REAL_ESTATE_TYPES_SUBE_TYPE(params) {
      return getSubeType(params).then((res) => {
        this.listingRealEstateTypesSubeType = res.data;
      });
    },

    CREATE_REAL_ESTATE_TYPES_SUBE_TYPE(data) {
      return createRealEstateTypesSubeType(data);
    },

    CREATE_SUB_TYPE_RELATION_FORM(data) {
      return createSubTypeRelationForm(data);
    },

    DELETE_SUB_TYPE_RELATION_FORM(params) {
      return deleteSubTypeRelationForm(params);
    },

    CHANGE_STATUS_SUB_TYPE_RELATION_FORM(params) {
      return changeStatusSubTypeRelationForm(params);
    },

    DELETE_SUBE_TYPE(id) {
      return deleteSubeType(id);
    },

    UPDATE_SUBE_TYPE(data) {
      return updateSubeType(data);
    },
  },

  //   GETTERS
  getters: {
    getSubeTypeList(state) {
      return state.listingRealEstateTypesSubeType;
    },

    getSubeTypeRelationFormList(state) {
      return state.listingTypesRelationFormsList;
    },
  },
});
