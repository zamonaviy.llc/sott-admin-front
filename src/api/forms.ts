import http from "@/plugins/axios";

export function createForms(data) {
  return http({
    method: "POST",
    url: "/forms/full_create",
    data,
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlcyI6W10sImlhdCI6MTY5MTk5NzM3Mn0.ov_kMYoqnzQbuKpUCmFqiEmt-9zpv7bqIXHWFfB8-PI`,
    },
  });
}

export function getFormsList(params) {
  return http({
    method: "GET",
    url: "/forms/list",
    params,
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlcyI6W10sImlhdCI6MTY5MTk5NzM3Mn0.ov_kMYoqnzQbuKpUCmFqiEmt-9zpv7bqIXHWFfB8-PI`,
    },
  });
}

export function getOneForm(id) {
  return http({
    method: "GET",
    url: `/forms/details`,
    params: { pk: id },
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlcyI6W10sImlhdCI6MTY5MTk5NzM3Mn0.ov_kMYoqnzQbuKpUCmFqiEmt-9zpv7bqIXHWFfB8-PI`,
    },
  });
}

export function updateOneForm({ data, id }) {
  return http({
    method: "PATCH",
    url: `/forms/full_update`,
    data,
    params: { pk: id },
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlcyI6W10sImlhdCI6MTY5MTk5NzM3Mn0.ov_kMYoqnzQbuKpUCmFqiEmt-9zpv7bqIXHWFfB8-PI`,
    },
  });
}
