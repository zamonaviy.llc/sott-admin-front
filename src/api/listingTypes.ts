import http from "@/plugins/axios";

export function createRealEstateTypesSubeType(data): object {
  return http({
    method: "POST",
    url: "/types/sub_types",
    data,
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlcyI6W10sImlhdCI6MTY5MTk5NzM3Mn0.ov_kMYoqnzQbuKpUCmFqiEmt-9zpv7bqIXHWFfB8-PI`,
    },
  });
}

export function createSubTypeRelationForm(data) {
  return http({
    method: "POST",
    url: "/forms/main_sub_form_relation",
    data,
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlcyI6W10sImlhdCI6MTY5MTk5NzM3Mn0.ov_kMYoqnzQbuKpUCmFqiEmt-9zpv7bqIXHWFfB8-PI`,
    },
  });
}

export function changeStatusSubTypeRelationForm(params) {
  return http({
    method: "PATCH",
    url: "/forms/main_sub_form_relation",
    params,
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlcyI6W10sImlhdCI6MTY5MTk5NzM3Mn0.ov_kMYoqnzQbuKpUCmFqiEmt-9zpv7bqIXHWFfB8-PI`,
    },
  });
}

export function deleteSubTypeRelationForm(params) {
  return http({
    method: "PATCH",
    url: "/forms/main_sub_form_relation/activation",
    params,
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlcyI6W10sImlhdCI6MTY5MTk5NzM3Mn0.ov_kMYoqnzQbuKpUCmFqiEmt-9zpv7bqIXHWFfB8-PI`,
    },
  });
}

export function getSubeType(params) {
  return http({
    method: "GET",
    url: "/types/sub_types",
    params,
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlcyI6W10sImlhdCI6MTY5MTk5NzM3Mn0.ov_kMYoqnzQbuKpUCmFqiEmt-9zpv7bqIXHWFfB8-PI`,
    },
  });
}

export function getSubeTypeRelationFormList(params) {
  return http({
    method: "GET",
    url: "/forms/list_by_types_shirt",
    params,
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlcyI6W10sImlhdCI6MTY5MTk5NzM3Mn0.ov_kMYoqnzQbuKpUCmFqiEmt-9zpv7bqIXHWFfB8-PI`,
    },
  });
}

export function deleteSubeType(type_id) {
  return http({
    method: "PATCH",
    url: `/types/sub_types/deactivate`,
    params: { type_id },
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlcyI6W10sImlhdCI6MTY5MTk5NzM3Mn0.ov_kMYoqnzQbuKpUCmFqiEmt-9zpv7bqIXHWFfB8-PI`,
    },
  });
}

export function updateSubeType(data) {
  return http({
    method: "PATCH",
    url: `/types/sub_types/update`,
    data,
    params: { type_id: data.type_id },
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIiLCJyb2xlcyI6W10sImlhdCI6MTY5MTk5NzM3Mn0.ov_kMYoqnzQbuKpUCmFqiEmt-9zpv7bqIXHWFfB8-PI`,
    },
  });
}
